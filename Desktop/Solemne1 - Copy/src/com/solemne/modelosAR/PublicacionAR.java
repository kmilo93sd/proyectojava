package com.solemne.modelosAR;

import com.solemne.conexion.ConexionJDBC;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PublicacionAR {
	private final ConexionJDBC con; 
	Connection cxn;
	
	private int id;
	private String imagen;
	private String video;
	private String notas;
	private int idSemana;
	
	public PublicacionAR(){
		con=new ConexionJDBC();
		cxn=con.getConexion();

	}
	
	//GETTERS Y SETTERS
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public int getIdSemana() {
		return idSemana;
	}

	public void setIdSemana(int idSemana) {
		this.idSemana = idSemana;
	}
	
	//OPERACIONES CRUD
	
	//INSERTAR
	public void insertar(PublicacionAR publicacion){
		String sql="INSERT INTO cro_publicacion(pub_id,pub_imagen,pub_video,pub_notas,pub_id_semana) VALUES (?,?,?,?,?)";
		try {
			PreparedStatement pStmt= cxn.prepareStatement(sql);
			pStmt.setInt(1, publicacion.getId());
			pStmt.setString(2, publicacion.getImagen());
			pStmt.setString(3, publicacion.getVideo());
			pStmt.setString(4, publicacion.getNotas());
			pStmt.setInt(5, publicacion.getIdSemana());
			int exec=pStmt.executeUpdate();
			if(exec==0){
				throw new SQLException();
			}
			pStmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//LEER
	public PublicacionAR leer(int id){
		PublicacionAR publicacion=null;
		String sql="SELECT * FROM cro_publicacion WHERE pub_id=?";
		try {
			PreparedStatement pStmt=cxn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs=pStmt.executeQuery();
			while(rs.next()){
				publicacion= new PublicacionAR();
				publicacion.setId(rs.getInt(1));
				publicacion.setImagen(rs.getString(2));
				publicacion.setVideo(rs.getString(3));
				publicacion.setNotas(rs.getString(4));
				publicacion.setIdSemana(rs.getInt(5));
			}
			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return publicacion;
	}
	
	//ACTUALIZAR
	public void actualizar(PublicacionAR publicacion){
		String sql = " UPDATE cro_publicacion SET pub_imagen= ?,pub_video= ?,pub_notas= ? WHERE pub_id = ? ";
		PreparedStatement pst;
		try {
			pst = cxn.prepareStatement(sql);
			pst.setString( 1, publicacion.getImagen());
			pst.setString( 2, publicacion.getVideo());
			pst.setString( 3, publicacion.getNotas());
			pst.setInt( 4, publicacion.getId());
			pst.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//ELIMINAR
	public void eliminar(int id){
		String sql = " DELETE FROM cro_publicacion WHERE pub_id = ?";
		PreparedStatement pst;
		try {
			pst = cxn.prepareStatement(sql);
			pst.setInt( 1, id);
			pst.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public List<PublicacionAR> leerAll(){
		//inicializa lista, query y el statement
		List <PublicacionAR>lista=null;
		String sql = "SELECT * FROM cro_publicacion";
		PreparedStatement pStmt;
		
		try {
			
			//prepara la consulta y ejecuta la query
			pStmt = cxn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();
			
			//crea la lista de categorias
			lista=new ArrayList <PublicacionAR>();
			while(rs.next()){
				PublicacionAR publicacion=new PublicacionAR();
				publicacion.setId(rs.getInt(1));
				publicacion.setImagen(rs.getString(2));
				publicacion.setVideo(rs.getString(3));
				publicacion.setNotas(rs.getString(4));
				publicacion.setIdSemana(rs.getInt(5));
				lista.add(publicacion);
				
			}
			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lista;
	}
	
	
	
	

}
