package com.solemne.modelosAR;

import com.solemne.conexion.ConexionJDBC;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsuarioAR {
	private final ConexionJDBC con; 
	Connection cxn;
	
	private int id;
	private String nombre;
	private String correo;
	private Boolean estado;
	private String pass;
	public UsuarioAR(){
		con=new ConexionJDBC();
		cxn=con.getConexion();

	}
	
	// GETTERS Y SETTERS
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public Boolean getEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	// OPERACIONES CRUD
	
	//INSERTAR
	public void insertar(UsuarioAR usuario){
		String sql="INSERT INTO cue_usuarios(usu_id,usu_nombre,usu_correo,usu_estado,usu_pass) VALUES (?,?,?,?,?)";
		try {
			PreparedStatement pStmt= cxn.prepareStatement(sql);
			pStmt.setInt(1, usuario.getId());
			pStmt.setString(2, usuario.getNombre());
			pStmt.setString(3, usuario.getCorreo());
			pStmt.setBoolean(4, usuario.getEstado());
			pStmt.setString(5, usuario.getPass());
			int exec=pStmt.executeUpdate();
			if(exec==0){
				throw new SQLException();
			}
			pStmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//LEER
	
	public UsuarioAR leer(int id){
		UsuarioAR usuario=null;
		String sql="SELECT * FROM cue_usuarios WHERE usu_id=?";
		try {
			PreparedStatement pStmt=cxn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs=pStmt.executeQuery();
			while(rs.next()){
				usuario= new UsuarioAR();
				usuario.setId(rs.getInt(1));
				usuario.setNombre(rs.getString(2));
				usuario.setCorreo(rs.getString(3));
				usuario.setEstado(rs.getBoolean(4));
				usuario.setPass(rs.getString(5));
			}
			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return usuario;
	}
	
	//ACTUALIZAR
	
	public void actualizar(UsuarioAR usuario){
		String sql = "UPDATE cue_usuarios SET usu_nombre=?,usu_correo=?,usu_estado=?,usu_pass = ? WHERE usu_id = ? ";
		PreparedStatement pst;
		try {
			pst = cxn.prepareStatement(sql);
			pst.setString( 1, usuario.getNombre());
			pst.setString( 2, usuario.getCorreo());
			pst.setBoolean( 3, usuario.getEstado());
			pst.setString( 4, usuario.getPass());
			pst.setInt(5,usuario.getId());
			pst.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//ELIMINAR
	
	public void eliminar(int id){
		String sql = " DELETE FROM cue_usuarios WHERE usu_id = ?";
		PreparedStatement pst;
		try {
			pst = cxn.prepareStatement(sql);
			pst.setInt( 1, id);
			pst.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public List<UsuarioAR> leerAll(){
		//inicializa lista, query y el statement
		List <UsuarioAR>lista=null;
		String sql = "SELECT * FROM cue_usuarios";
		PreparedStatement pStmt;
		
		try {
			
			//prepara la consulta y ejecuta la query
			pStmt = cxn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();
			
			//crea la lista de categorias
			lista=new ArrayList <UsuarioAR>();
			while(rs.next()){
				UsuarioAR usuario=new UsuarioAR();
				usuario.setId(rs.getInt(1));
				usuario.setNombre(rs.getString(2));
				usuario.setCorreo(rs.getString(3));
				usuario.setEstado(rs.getBoolean(4));
				usuario.setPass(rs.getString(5));
				lista.add(usuario);
			}
			pStmt.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		
		return lista;
	}
	
}
