package com.solemne.modelosAR;

import com.solemne.conexion.ConexionJDBC;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SemanaAR {
	private final ConexionJDBC con; 
	Connection cxn;
	
	private int id;
	private String titulo;
	private String detalle;
	private int idCultivo;
	
	public SemanaAR(){
		con=new ConexionJDBC();
		cxn=con.getConexion();

	}
	
	//GETTERS Y SETTERS
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public int getIdCultivo() {
		return idCultivo;
	}

	public void setIdCultivo(int idCultivo) {
		this.idCultivo = idCultivo;
	}

	//OPERACIONES CRUD
	
	//INSERTAR
	public void insertar(SemanaAR semana){
		String sql="INSERT INTO cro_semanas(sem_id,sem_titulo,sem_detalle,sem_id_cultivo) VALUES (?,?,?,?)";
		try {
			PreparedStatement pStmt= cxn.prepareStatement(sql);
			pStmt.setInt(1, semana.getId());
			pStmt.setString(2, semana.getTitulo());
			pStmt.setString(3, semana.getDetalle());
			pStmt.setInt(4, semana.getIdCultivo());
			int exec=pStmt.executeUpdate();
			if(exec==0){
				throw new SQLException();
			}
			pStmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//LEER
	public SemanaAR leer(int id){
		SemanaAR semana=null;
		String sql="SELECT * FROM cro_semanas WHERE sem_id=?";
		try {
			PreparedStatement pStmt=cxn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs=pStmt.executeQuery();
			while(rs.next()){
				semana= new SemanaAR();
				semana.setId(rs.getInt(1));
				semana.setTitulo(rs.getString(2));
				semana.setDetalle(rs.getString(3));
				semana.setIdCultivo(rs.getInt(4));
			}
			rs.close();
			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return semana;
	}
	
	//ACTUALIZAR
	public void actualizar(SemanaAR semana){
		String sql = " UPDATE cro_semanas SET sem_titulo= ?,sem_detalle = ? WHERE sem_id = ? ";
		PreparedStatement pst;
		try {
			pst = cxn.prepareStatement(sql);
			pst.setString( 1, semana.getTitulo());
			pst.setString( 2, semana.getDetalle());
			pst.setInt( 3, semana.getId());
			pst.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//ELIMINAR
	public void eliminar(int id){
		String sql = " DELETE FROM cro_semanas WHERE sem_id = ?";
		PreparedStatement pst;
		try {
			pst = cxn.prepareStatement(sql);
			pst.setInt( 1, id);
			pst.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public List<SemanaAR> leerAll(){
		//inicializa lista, query y el statement
		List <SemanaAR>lista=null;
		String sql = "SELECT * FROM cro_semanas";
		PreparedStatement pStmt;
		
		try {
			
			//prepara la consulta y ejecuta la query
			pStmt = cxn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();
			
			//crea la lista de categorias
			lista=new ArrayList <SemanaAR>();
			while(rs.next()){
				SemanaAR semana=new SemanaAR();
				semana.setId(rs.getInt(1));
				semana.setTitulo(rs.getString(2));
				semana.setDetalle(rs.getString(3));
				semana.setIdCultivo(rs.getInt(4));
				lista.add(semana);
				
			}
			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lista;
	}
	
}
