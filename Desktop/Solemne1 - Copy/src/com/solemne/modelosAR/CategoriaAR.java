package com.solemne.modelosAR;

import com.solemne.conexion.ConexionJDBC;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



public class CategoriaAR {
	private final ConexionJDBC con; 
	Connection cxn;
	private int id;  
	private String nombre;
	
	public CategoriaAR(){
		con=new ConexionJDBC();
		cxn=con.getConexion();
	}
	
	
	//getters y setters
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	//Operaciones CRUD
	
	
	// MOSTRAR TODAS LAS CATEGORIAS
	public List<CategoriaAR> leerAll(){
		//inicializa lista, query y el statement
		List <CategoriaAR>lista=null;
		String sql = "SELECT * FROM ind_categorias";
		PreparedStatement pStmt;
		
		try {
			
			//prepara la consulta y ejecuta la query
			pStmt = cxn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();
			
			//crea la lista de categorias
			lista=new ArrayList <CategoriaAR>();
			while(rs.next()){
				CategoriaAR categoria=new CategoriaAR();
				categoria.setId(rs.getInt(1));
				categoria.setNombre(rs.getString(2));
				lista.add(categoria);
				
			}
			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lista;
	}

	// MOSTRAR CATEGORIA POR ID
	public CategoriaAR leer(int id){
		CategoriaAR categoria=null;
		String sql="SELECT * FROM ind_categorias WHERE cat_id=?";
		try {
			PreparedStatement pStmt=cxn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs=pStmt.executeQuery();
			while(rs.next()){
				categoria= new CategoriaAR();
				categoria.setId(rs.getInt(1));
				categoria.setNombre(rs.getString(2));
			}
			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return categoria;
	}
	
	//INSERTAR DATOS
	public void insertar(CategoriaAR categoria){
		String sql="INSERT INTO ind_categorias(cat_id,cat_nombre) VALUES (?,?)";
		try {
			PreparedStatement pStmt= cxn.prepareStatement(sql);
			pStmt.setInt(1, categoria.getId());
			pStmt.setString(2, categoria.getNombre());
			int exec=pStmt.executeUpdate();
			if(exec==0){
				throw new SQLException();
			}
			pStmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//ACTUALIZAR DATOS
	public void actualizar(CategoriaAR categoria){
		String sql = " UPDATE ind_categorias SET cat_nombre = ? WHERE cat_id = ? ";
		PreparedStatement pst;
		try {
			pst = cxn.prepareStatement(sql);
			pst.setString( 1, categoria.getNombre());
			pst.setInt( 2, categoria.getId());
			pst.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//ELIMINAR DATOS POR ID
	public void eliminar(int id){
		String sql = " DELETE FROM ind_categorias WHERE cat_id = ?";
		PreparedStatement pst;
		try {
			pst = cxn.prepareStatement(sql);
			pst.setInt( 1, id);
			pst.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	
	

}
