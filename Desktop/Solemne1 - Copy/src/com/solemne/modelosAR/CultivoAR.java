package com.solemne.modelosAR;

import com.solemne.conexion.ConexionJDBC;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CultivoAR {
	private final ConexionJDBC con; 
	Connection cxn;
	private int id;
	private String nombre;
	private boolean visibilidad;
	private String info;
	private int idUsuario;
	private int idCategoria;
	public CultivoAR(){
		con=new ConexionJDBC();
		cxn=con.getConexion();
	}
	
	
	//getters y setters
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public boolean isVisibilidad() {
		return visibilidad;
	}


	public void setVisibilidad(boolean visibilidad) {
		this.visibilidad = visibilidad;
	}


	public String getInfo() {
		return info;
	}


	public void setInfo(String info) {
		this.info = info;
	}


	public int getIdUsuario() {
		return idUsuario;
	}


	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}


	public int getIdCategoria() {
		return idCategoria;
	}


	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}
	
	
	//Operaciones CRUD
	// MOSTRAR CULTIVO POR ID
		public CultivoAR leer(int id){
			CultivoAR cultivo=null;
			String sql="SELECT * FROM cro_cultivos WHERE cul_id=?";
			try {
				PreparedStatement pStmt=cxn.prepareStatement(sql);
				pStmt.setInt(1, id);
				ResultSet rs=pStmt.executeQuery();
				while(rs.next()){
					cultivo= new CultivoAR();
					cultivo.setId(rs.getInt(1));
					cultivo.setNombre(rs.getString(2));
					cultivo.setVisibilidad(rs.getBoolean(3));
					cultivo.setInfo(rs.getString(4));
					cultivo.setIdUsuario(rs.getInt(5));
					cultivo.setIdCategoria(rs.getInt(6));
				}
				pStmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return cultivo;
		}
		
		//INSERTAR DATOS
		public void insertar(CultivoAR cultivo){
			String sql="INSERT INTO cro_cultivos(cul_id,cul_nombre,cul_visibilidad,cul_info,cul_id_usuario,cul_id_categoria) VALUES (?,?,?,?,?,?)";
			try {
				PreparedStatement pStmt= cxn.prepareStatement(sql);
				pStmt.setInt(1, cultivo.getId());
				pStmt.setString(2, cultivo.getNombre());
				pStmt.setBoolean(3, cultivo.isVisibilidad());
				pStmt.setString(4, cultivo.getInfo());
				pStmt.setInt(5, cultivo.getIdUsuario());
				pStmt.setInt(6, cultivo.getIdCategoria());
				int exec=pStmt.executeUpdate();
				if(exec==0){
					throw new SQLException();
				}
				pStmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		//ACTUALIZAR DATOS
		public void actualizar(CultivoAR cultivo){
			String sql = " UPDATE cro_cultivos	SET  cul_nombre=?, cul_visibilidad=?, cul_info=?, cul_id_usuario=?,cul_id_categoria=? 	WHERE cul_id=?";
			PreparedStatement pst;
			try {
				pst = cxn.prepareStatement(sql);
				pst.setString( 1, cultivo.getNombre());
				pst.setBoolean( 2, cultivo.isVisibilidad());
				pst.setString( 3, cultivo.getInfo());
				pst.setInt( 4, cultivo.getIdUsuario());
				pst.setInt( 5, cultivo.getIdCategoria());
				pst.setInt( 6, cultivo.getId());
				pst.execute();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		//ELIMINAR DATOS POR ID
		public void eliminar(int id){
			String sql = " DELETE FROM cro_cultivos WHERE cul_id = ?";
			PreparedStatement pst;
			try {
				pst = cxn.prepareStatement(sql);
				pst.setInt( 1, id);
				pst.execute();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		public List<CultivoAR> leerAll(){
			//inicializa lista, query y el statement
			List <CultivoAR>lista=null;
			String sql = "SELECT * FROM cro_cultivos";
			PreparedStatement pStmt;
			
			try {
				
				//prepara la consulta y ejecuta la query
				pStmt = cxn.prepareStatement(sql);
				ResultSet rs = pStmt.executeQuery();
				
				//crea la lista de categorias
				lista=new ArrayList <CultivoAR>();
				while(rs.next()){
					CultivoAR cultivo=new CultivoAR();
					cultivo.setId(rs.getInt(1));
					cultivo.setNombre(rs.getString(2));
					cultivo.setVisibilidad(rs.getBoolean(3));
					cultivo.setInfo(rs.getString(4));
					cultivo.setIdUsuario(rs.getInt(5));
					cultivo.setIdCategoria(rs.getInt(6));
					lista.add(cultivo);
					
				}
				pStmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return lista;
		}
		
		
		
		
		
	
}
