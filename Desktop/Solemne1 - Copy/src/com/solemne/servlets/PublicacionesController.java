package com.solemne.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.solemne.modelosAR.PublicacionAR;

/**
 * Servlet implementation class PublicacionesController
 */
@WebServlet("/PublicacionesController")
public class PublicacionesController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PublicacionesController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PublicacionAR publicacion = new PublicacionAR();
		switch(request.getParameter("accion")){
		case "crear":
			publicacion.setId(Integer.parseInt(request.getParameter("id")));
			publicacion.setImagen(request.getParameter("imagen"));
			publicacion.setVideo(request.getParameter("video"));
			publicacion.setNotas(request.getParameter("notas"));
			publicacion.setIdSemana(Integer.parseInt(request.getParameter("idSemana")));
			publicacion.insertar(publicacion);
			response.sendRedirect("PublicacionViews/PublicacionCrud.jsp");
			break;
		case "editar":
			publicacion.setId(Integer.parseInt(request.getParameter("id")));
			publicacion.setImagen(request.getParameter("imagen"));
			publicacion.setVideo(request.getParameter("video"));
			publicacion.setNotas(request.getParameter("notas"));
			publicacion.setIdSemana(Integer.parseInt(request.getParameter("idSemana")));
			publicacion.actualizar(publicacion);
			response.sendRedirect("PublicacionViews/PublicacionCrud.jsp");
			break;
		case "leer":
			publicacion=publicacion.leer(Integer.parseInt(request.getParameter("id")));
			request.getSession().setAttribute("id", publicacion.getId());
			request.getSession().setAttribute("imagen", publicacion.getImagen());
			request.getSession().setAttribute("video", publicacion.getVideo());
			request.getSession().setAttribute("notas", publicacion.getNotas());
			request.getSession().setAttribute("idSemana", publicacion.getIdSemana());
			response.sendRedirect("PublicacionViews/PublicacionLeer.jsp");
			break;
		case "eliminar":
			publicacion.eliminar(Integer.parseInt(request.getParameter("id")));
			response.sendRedirect("PublicacionViews/PublicacionCrud.jsp");
			
			break;
		}
	}

}
