package com.solemne.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.solemne.modelosAR.UsuarioAR;

/**
 * Servlet implementation class UsuariosController
 */
@WebServlet("/UsuariosController")
public class UsuariosController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public UsuariosController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UsuarioAR usuario = new UsuarioAR();
		switch(request.getParameter("accion")){
		case "crear":
			usuario.setId(Integer.parseInt(request.getParameter("id")));
			usuario.setNombre(request.getParameter("nombre"));
			usuario.setCorreo(request.getParameter("correo"));
			if("on".equals(request.getParameter("estado"))){
				usuario.setEstado(true);
			}else{
				usuario.setEstado(false);
			}
			usuario.setPass(request.getParameter("pass"));
			usuario.insertar(usuario);
			response.sendRedirect("UsuarioViews/UsuarioCrud.jsp");
			break;
		case "editar":
			usuario.setId(Integer.parseInt(request.getParameter("id")));
			usuario.setNombre(request.getParameter("nombre"));
			usuario.setCorreo(request.getParameter("correo"));
			if("on".equals(request.getParameter("estado"))){
				usuario.setEstado(true);
			}else{
				usuario.setEstado(false);
			}
			usuario.setPass(request.getParameter("pass"));
			usuario.actualizar(usuario);
			response.sendRedirect("UsuarioViews/UsuarioCrud.jsp");
			break;
		case "leer":
			
			usuario=usuario.leer(Integer.parseInt(request.getParameter("id")));
			request.getSession().setAttribute("id", usuario.getId());
			request.getSession().setAttribute("nombre", usuario.getNombre());
			request.getSession().setAttribute("correo", usuario.getCorreo());
			request.getSession().setAttribute("estado", usuario.getEstado()?"Publico":"Oculto");
			request.getSession().setAttribute("pass", usuario.getPass());
			response.sendRedirect("UsuarioViews/UsuarioLeer.jsp");
			break;
		case "eliminar":
			usuario.eliminar(Integer.parseInt(request.getParameter("id")));
			response.sendRedirect("UsuarioViews/UsuarioCrud.jsp");
			
			break;
		}
		
	}

}
