package com.solemne.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.solemne.modelosAR.CategoriaAR;

/**
 * Servlet implementation class CategoriasController
 */
@WebServlet("/CategoriasController")
public class CategoriasController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CategoriasController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		CategoriaAR categoria= new CategoriaAR();
		switch(request.getParameter("accion")){
		case "crear":
			categoria.setId(Integer.parseInt(request.getParameter("id")));
			categoria.setNombre(request.getParameter("nombre"));
			categoria.insertar(categoria);
			response.sendRedirect("CategoriaViews/CategoriaCrud.jsp");
			break;
		case "leer":
			categoria=categoria.leer(Integer.parseInt(request.getParameter("id")));
			request.getSession().setAttribute("id", categoria.getId());
			request.getSession().setAttribute("nombre", categoria.getNombre());
			response.sendRedirect("CategoriaViews/CategoriaLeer.jsp");
			break;
		case "editar":
			categoria.setId(Integer.parseInt(request.getParameter("id")));
			categoria.setNombre(request.getParameter("nombre"));
			categoria.actualizar(categoria);
			response.sendRedirect("CategoriaViews/CategoriaCrud.jsp");
			break;
			
		case "eliminar":
			categoria.eliminar(Integer.parseInt(request.getParameter("id")));
			response.sendRedirect("CategoriaViews/CategoriaCrud.jsp");
			break;
		}
		
	}

}
