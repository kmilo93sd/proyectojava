package com.solemne.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.solemne.modelosAR.SemanaAR;

/**
 * Servlet implementation class SemanasController
 */
@WebServlet("/SemanasController")
public class SemanasController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SemanasController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SemanaAR semana = new SemanaAR();
		switch(request.getParameter("accion")){
		case "crear":
			semana.setId(Integer.parseInt(request.getParameter("id")));
			semana.setTitulo(request.getParameter("titulo"));
			semana.setDetalle(request.getParameter("detalle"));
			semana.setIdCultivo(Integer.parseInt(request.getParameter("idCultivo")));
			semana.insertar(semana);
			response.sendRedirect("SemanaViews/SemanaCrud.jsp");
			break;
		case "editar":
			semana.setId(Integer.parseInt(request.getParameter("id")));
			semana.setTitulo(request.getParameter("titulo"));
			semana.setDetalle(request.getParameter("detalle"));
			semana.setIdCultivo(Integer.parseInt(request.getParameter("idCultivo")));
			semana.actualizar(semana);
			response.sendRedirect("SemanaViews/SemanaCrud.jsp");
			break;
		case "leer":
			semana=semana.leer(Integer.parseInt(request.getParameter("id")));
			request.getSession().setAttribute("id", semana.getId());
			request.getSession().setAttribute("titulo", semana.getTitulo());
			request.getSession().setAttribute("detalle", semana.getDetalle());
			request.getSession().setAttribute("idCultivo", semana.getIdCultivo());
			response.sendRedirect("SemanaViews/SemanaLeer.jsp");
			break;
		case "eliminar":
			semana.eliminar(Integer.parseInt(request.getParameter("id")));
			response.sendRedirect("SemanaViews/SemanaCrud.jsp");
			
			break;
		}
	}

}
