package com.solemne.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.solemne.modelosAR.CultivoAR;

/**
 * Servlet implementation class CultivosController
 */
@WebServlet("/CultivosController")
public class CultivosController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CultivosController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CultivoAR cultivo = new CultivoAR();
		switch(request.getParameter("accion")){
		case "crear":
			cultivo.setId(Integer.parseInt(request.getParameter("id")));
			cultivo.setNombre(request.getParameter("nombre"));
			if("on".equals(request.getParameter("estado"))){
				cultivo.setVisibilidad(true);
			}else{
				cultivo.setVisibilidad(false);
			}
			cultivo.setInfo(request.getParameter("info"));
			cultivo.setIdUsuario(Integer.parseInt(request.getParameter("idUser")));
			cultivo.setIdCategoria(Integer.parseInt(request.getParameter("idCat")));
			cultivo.insertar(cultivo);
			response.sendRedirect("CultivoViews/CultivoCrud.jsp");
			break;
		case "editar":
			cultivo.setId(Integer.parseInt(request.getParameter("id")));
			cultivo.setNombre(request.getParameter("nombre"));
			if("on".equals(request.getParameter("estado"))){
				cultivo.setVisibilidad(true);
			}else{
				cultivo.setVisibilidad(false);
			}
			cultivo.setInfo(request.getParameter("info"));
			cultivo.setIdUsuario(Integer.parseInt(request.getParameter("idUser")));
			cultivo.setIdCategoria(Integer.parseInt(request.getParameter("idCat")));
			cultivo.actualizar(cultivo);
			response.sendRedirect("CultivoViews/CultivoCrud.jsp");
			break;
		case "leer":
			cultivo=cultivo.leer(Integer.parseInt(request.getParameter("id")));
			request.getSession().setAttribute("id", cultivo.getId());
			request.getSession().setAttribute("nombre", cultivo.getNombre());
			request.getSession().setAttribute("visibilidad", cultivo.isVisibilidad()?"Publico":"Privado");
			request.getSession().setAttribute("info", cultivo.getInfo());
			request.getSession().setAttribute("idUser", cultivo.getIdUsuario());
			request.getSession().setAttribute("idCat", cultivo.getIdCategoria());
			response.sendRedirect("CultivoViews/CultivoLeer.jsp");
			break;
		case "eliminar":
			cultivo.eliminar(Integer.parseInt(request.getParameter("id")));
			response.sendRedirect("CultivoViews/CultivoCrud.jsp");
			
			break;
		}
	}

}
