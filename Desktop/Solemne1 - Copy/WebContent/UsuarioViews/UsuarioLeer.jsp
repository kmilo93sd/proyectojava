<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
	integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
	crossorigin="anonymous">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
	integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
	integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	crossorigin="anonymous"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div class="container">
    <h3>Crear Usuario</h3>
		<form action="../UsuariosController" method="POST">
			<table class="table">
	 			<tbody>
	    			<tr>
	      				<td><label>ID</label></td>
	      				<td><label><%=session.getAttribute("id") %></label><input type="hidden" name="id" value="<%=session.getAttribute("id") %>"></td>
	    			</tr>
	    			<tr>
	      				<td><label>Nombre</label></td>
	      				<td><input type="text" name="nombre"  value="<%=session.getAttribute("nombre") %>"></td>
	    			</tr>
			    	<tr>
			      		<td><label>Correo</label></td>
			      		<td><input type="text" name="correo"  value="<%=session.getAttribute("correo") %>"></td>
			    	</tr>
			    	<tr>
			      		<td><label>Estado</label></td>
			      		<td><%=session.getAttribute("estado") %><br>
			      			<label class="radio-inline"><input type="radio" name="estado" value="on">Publico</label>
							<label class="radio-inline"><input type="radio" name="estado" value="off">Oculto</label>
			      		</td>
			    	</tr>
			    	<tr>
			      		<td><label>Contraseņa</label></td>
			      		<td><input type="password" name="pass"  value="<%=session.getAttribute("pass") %>"></td>
			    	</tr>
			    </tbody>
		    </table>
			
			<br>
			<br>
			
			
			<br>
			<button type="submit" class="btn btn-primary" name="accion" value="editar">Editar</button>
			<a class="btn btn-primary" href="UsuarioCrud.jsp" role="button">Salir</a>
		</form>
		

</div>
	
</body>
</html>