<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.solemne.modelosAR.CategoriaAR"%>
<%@ page import="java.util.ArrayList"%> 
<%@ page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
	integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
	crossorigin="anonymous">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
	integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
	integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	crossorigin="anonymous"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<h2>Crud Categoria</h2>
			</div>
			<div class="col-md-4"></div>
		</div>
		<div class="row">
			
			<div class="col-md-5">
				<h2>Leer</h2><p>
				<form action="../CategoriasController" method="POST">
					<input type="text" name="id" placeholder="id">
					<button type="submit" class="btn btn-primary" name="accion" value="leer">Leer</button>
				</form>
				<p>
				<a class="btn btn-primary" href="CategoriaInsertar.jsp" role="button">Nueva Categoria</a><p>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-6">
				<table class="table">
					<thead>
						<tr>
							<th>Id</th>
							<th>Nombre</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<% 
							List<CategoriaAR> listado= new ArrayList<CategoriaAR>();
						CategoriaAR objeto= new CategoriaAR();
							listado=objeto.leerAll();
						for(CategoriaAR elemento: listado){
							%>
							<tr>
						 		<td><%=elemento.getId() %></td>
								<td><%=elemento.getNombre()%></td>
								<td>
									<form action="../CategoriasController" method="POST">
										<input type="hidden" name="id" value="<%=elemento.getId()%>">
										<button type="submit" class="btn btn-primary" name="accion" value="leer">Leer</button>
									</form>
								</td>
								<td>
									<form action="../CategoriasController" method="POST">
										<input type="hidden" name="id" value="<%=elemento.getId()%>">
										<button type="submit" class="btn btn-danger" name="accion" value="eliminar">Eliminar</button>
									</form>
								</td>
						 	</tr> 
						<%
						}
						%>
							
							
						
						 					
												

					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>
		
	<a class="btn btn-primary" href="../Index.jsp" role="button">Salir</a>
	</div>
</body>
</html>