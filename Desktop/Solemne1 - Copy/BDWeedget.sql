
/* Drop Tables */

DROP TABLE IF EXISTS cro_publicacion;
DROP TABLE IF EXISTS cro_semanas;
DROP TABLE IF EXISTS cro_cultivos;
DROP TABLE IF EXISTS cue_follows;
DROP TABLE IF EXISTS cue_seguidores;
DROP TABLE IF EXISTS cue_usuarios;
DROP TABLE IF EXISTS ind_categorias;




/* Create Tables */

-- tabla que contiene los cultivos, pertenece al modulo de crop
CREATE TABLE cro_cultivos
(
	-- id del cultivo
	cul_id int NOT NULL,
	-- nombre del cultivo
	cul_nombre varchar(70) NOT NULL,
	-- visibilidad del cultivo (visible, oculto)
	cul_visibilidad boolean NOT NULL,
	-- informacion sobre el cultivo
	cul_info varchar,
	-- id del usuario
	cul_id_usuario int NOT NULL,
	-- id categoria
	cul_id_categoria int NOT NULL,
	PRIMARY KEY (cul_id)
) WITHOUT OIDS;


-- tabla que contiene informacion de las publicaciones, pertenece al modulo crop
CREATE TABLE cro_publicacion
(
	-- id del archivo media
	pub_id int NOT NULL,
	-- imagenes
	pub_imagen varchar,
	-- video
	pub_video varchar,
	-- notas de la publicacion
	pub_notas varchar,
	-- id de la semana
	-- 
	pub_id_semana int NOT NULL,
	PRIMARY KEY (pub_id)
) WITHOUT OIDS;


CREATE TABLE cro_semanas
(
	-- id de la semana
	-- 
	sem_id int NOT NULL,
	-- titulo de la semana
	sem_titulo varchar(30) NOT NULL,
	-- detalles de la semana
	sem_detalle varchar,
	-- id del cultivo
	sem_id_cultivo int NOT NULL,
	PRIMARY KEY (sem_id)
) WITHOUT OIDS;


-- tabla que contiene el id de los usuarios seguidos
CREATE TABLE cue_follows
(
	-- id del usuario que sigues 
	fol_id int NOT NULL,
	-- id del usuario
	usu_id int NOT NULL,
	PRIMARY KEY (fol_id)
) WITHOUT OIDS;


-- tabla de seguidores que pertenece al modulo de cuenta
CREATE TABLE cue_seguidores
(
	-- id del seguidor
	seg_id int NOT NULL,
	-- id del usuario
	seg_id_usuario int NOT NULL,
	PRIMARY KEY (seg_id)
) WITHOUT OIDS;


-- tabla perteneciente al modulo de cuenta, contiene la informacion de la cuenta del usuario
CREATE TABLE cue_usuarios
(
	-- id del usuario
	usu_id int NOT NULL,
	-- nombre que usar� el usuario
	usu_nombre varchar(20) NOT NULL UNIQUE,
	-- Correo de registro del usuario
	usu_correo varchar(30) NOT NULL UNIQUE,
	-- estado del usuario (visible, oculto)
	usu_estado boolean NOT NULL,
	-- contrase�a de la cuenta
	usu_pass varchar(30) NOT NULL,
	PRIMARY KEY (usu_id)
) WITHOUT OIDS;


CREATE TABLE ind_categorias
(
	-- id categoria
	cat_id int NOT NULL,
	-- nombre categoria
	cat_nombre varchar(15) NOT NULL UNIQUE,
	PRIMARY KEY (cat_id)
) WITHOUT OIDS;



/* Create Foreign Keys */

ALTER TABLE cro_semanas
	ADD CONSTRAINT r_cultivo_semana FOREIGN KEY (sem_id_cultivo)
	REFERENCES cro_cultivos (cul_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
;


ALTER TABLE cro_publicacion
	ADD CONSTRAINT r_semana_publicacion FOREIGN KEY (pub_id_semana)
	REFERENCES cro_semanas (sem_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
;


ALTER TABLE cro_cultivos
	ADD CONSTRAINT r_usuario_cultivo FOREIGN KEY (cul_id_usuario)
	REFERENCES cue_usuarios (usu_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
;


ALTER TABLE cue_follows
	ADD CONSTRAINT r_usuario_follow FOREIGN KEY (usu_id)
	REFERENCES cue_usuarios (usu_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
;


ALTER TABLE cue_seguidores
	ADD CONSTRAINT r_usuario_seguidor FOREIGN KEY (seg_id_usuario)
	REFERENCES cue_usuarios (usu_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
;


ALTER TABLE cro_cultivos
	ADD FOREIGN KEY (cul_id_categoria)
	REFERENCES ind_categorias (cat_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



/* Comments */

COMMENT ON TABLE cro_cultivos IS 'tabla que contiene los cultivos, pertenece al modulo de crop';
COMMENT ON COLUMN cro_cultivos.cul_id IS 'id del cultivo';
COMMENT ON COLUMN cro_cultivos.cul_nombre IS 'nombre del cultivo';
COMMENT ON COLUMN cro_cultivos.cul_visibilidad IS 'visibilidad del cultivo (visible, oculto)';
COMMENT ON COLUMN cro_cultivos.cul_info IS 'informacion sobre el cultivo';
COMMENT ON COLUMN cro_cultivos.cul_id_usuario IS 'id del usuario';
COMMENT ON COLUMN cro_cultivos.cul_id_categoria IS 'id categoria';
COMMENT ON TABLE cro_publicacion IS 'tabla que contiene informacion de las publicaciones, pertenece al modulo crop';
COMMENT ON COLUMN cro_publicacion.pub_id IS 'id del archivo media';
COMMENT ON COLUMN cro_publicacion.pub_imagen IS 'imagenes';
COMMENT ON COLUMN cro_publicacion.pub_video IS 'video';
COMMENT ON COLUMN cro_publicacion.pub_notas IS 'notas de la publicacion';
COMMENT ON COLUMN cro_publicacion.pub_id_semana IS 'id de la semana
';
COMMENT ON COLUMN cro_semanas.sem_id IS 'id de la semana
';
COMMENT ON COLUMN cro_semanas.sem_titulo IS 'titulo de la semana';
COMMENT ON COLUMN cro_semanas.sem_detalle IS 'detalles de la semana';
COMMENT ON COLUMN cro_semanas.sem_id_cultivo IS 'id del cultivo';
COMMENT ON TABLE cue_follows IS 'tabla que contiene el id de los usuarios seguidos';
COMMENT ON COLUMN cue_follows.fol_id IS 'id del usuario que sigues ';
COMMENT ON COLUMN cue_follows.usu_id IS 'id del usuario';
COMMENT ON TABLE cue_seguidores IS 'tabla de seguidores que pertenece al modulo de cuenta';
COMMENT ON COLUMN cue_seguidores.seg_id IS 'id del seguidor';
COMMENT ON COLUMN cue_seguidores.seg_id_usuario IS 'id del usuario';
COMMENT ON TABLE cue_usuarios IS 'tabla perteneciente al modulo de cuenta, contiene la informacion de la cuenta del usuario';
COMMENT ON COLUMN cue_usuarios.usu_id IS 'id del usuario';
COMMENT ON COLUMN cue_usuarios.usu_nombre IS 'nombre que usar� el usuario';
COMMENT ON COLUMN cue_usuarios.usu_correo IS 'Correo de registro del usuario';
COMMENT ON COLUMN cue_usuarios.usu_estado IS 'estado del usuario (visible, oculto)';
COMMENT ON COLUMN cue_usuarios.usu_pass IS 'contrase�a de la cuenta';
COMMENT ON COLUMN ind_categorias.cat_id IS 'id categoria';
COMMENT ON COLUMN ind_categorias.cat_nombre IS 'nombre categoria';



